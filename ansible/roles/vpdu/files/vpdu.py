#!/usr/bin/env python3

import argparse
from datetime import datetime
import enum
import os
import re
import socketserver
import struct
import subprocess
import shutil
import sys
import threading
import time
import traceback

BRIDGE = 'virbr0'
SALAD_TCP_CONSOLE_PORT = int(os.getenv("SALAD_TCPCONSOLE_PORT", 8100))
DEFAULT_MACHINE_CFG = "x86_64-bios-tftp"
NUM_PORTS = 8
DUT_DISK_SIZE = '4G'
VPDU_DIR = os.getenv("VPDU_DIR", "./tmp_vpdu")
os.makedirs(VPDU_DIR, exist_ok=True)
OUTLETS = []
LOG = False


def log(msg, *args):
    if LOG:
        print(msg % args)


# Returns True if the CPU supports virtualization, else False
def has_vt():
    # TODO: Add support for ARM-based hosts
    p = re.compile("vmx|svm")
    with open("/proc/cpuinfo", "r") as f:
        return p.search(f.read()) is not None


class PowerState(enum.IntEnum):
    ON = 3
    OFF = 4
    ERROR = 5


class Architecture(enum.Enum):
    X86_64 = enum.auto()
    ARM64 = enum.auto()


class Firmware(enum.Enum):
    BIOS = enum.auto()
    EFI = enum.auto()


class NetbootProtocol(enum.Enum):
    TFTP = enum.auto()


def gen_mac(index):
    counter = format(index, '02x')
    return f'52:54:00:11:22:{counter}'


class DUT:
    def __init__(self, mac, fw_var_filename, disk_filename, machine_cfg):
        self.mac = mac
        self.fw_var_filename = fw_var_filename
        self.disk_filename = disk_filename
        self.machine_cfg = machine_cfg

        self.id = mac.replace(":", "")
        self.qemu = None

        self.thread = None
        self.stop_event = threading.Event()

    @property
    def state(self):
        if self.thread is not None and self.thread.is_alive():
            return PowerState.ON
        else:
            return PowerState.OFF

    def wait_for_shutdown(self):
        i = 0
        while self.state == PowerState.ON:
            i += 1
            if i == 10:
                raise ValueError("The qemu process got stuck in the shutdown procedure")
            else:
                time.sleep(0.5)

    def start(self):
        self.wait_for_shutdown()

        self.stop_event.clear()
        self.thread = threading.Thread(target=self.run)
        self.thread.start()

    def run(self):
        # Since we don't want the port state to revert back to OFF if we fail
        # to start the virtual machine, let's capture all exceptions then
        # unconditionally wait for the stop event.
        try:
            # Create the disk if it is missing
            if not os.path.exists(self.disk_filename):
                subprocess.run(['qemu-img', 'create', '-f', 'qcow2', self.disk_filename, DUT_DISK_SIZE],
                               stdout=subprocess.DEVNULL)

            extra_cmds = []
            if self.machine_cfg.arch == Architecture.X86_64:
                qemu_binary = "qemu-system-x86_64"
                if has_vt():
                    extra_cmds += ['-machine', 'q35,accel=kvm']
                else:
                    log('WARN: CPU VT support *not* detected... the DUT VM '
                        'performance will suffer!')

                if self.machine_cfg.firmware == Firmware.EFI:
                    # Copy the firmware variable file, so that we always start fresh!
                    shutil.copy("/usr/share/ovmf/x64/OVMF_VARS.fd", self.fw_var_filename, follow_symlinks=False)

                    extra_cmds += [
                        '-drive', "if=pflash,format=raw,readonly=on,file=/usr/share/ovmf/x64/OVMF_CODE.fd",
                        '-drive', f"if=pflash,format=raw,file={self.fw_var_filename}"
                    ]
                else:
                    # Nothing to do
                    pass
            elif self.machine_cfg.arch == Architecture.ARM64:
                qemu_binary = "qemu-system-aarch64"
                extra_cmds += [
                    '-machine', 'virt',
                    '-cpu', 'max',
                ]
                # TODO: Add support for KVM

                if self.machine_cfg.firmware == Firmware.EFI:
                    # Copy the firmware variable file, so that we always start fresh!
                    shutil.copy("/usr/share/ovmf/aarch64/QEMU_VARS.fd", self.fw_var_filename, follow_symlinks=False)

                    extra_cmds += [
                        '-drive', "if=pflash,format=raw,readonly=on,file=/usr/share/ovmf/aarch64/QEMU_CODE.fd",
                        '-drive', f"if=pflash,format=raw,file={self.fw_var_filename}"
                    ]
                else:
                    raise ValueError("Unsupported firmware type for the arm64 architecture")
            else:
                raise ValueError("Unsupported architecture")

            log_name = datetime.now().strftime(f'{VPDU_DIR}/dut-log-{self.id}-%H%M%S-%d%m%Y.log')
            cmd = [
                qemu_binary,
                '-m', '1024',
                '-smp', '2,sockets=2,cores=1,threads=1',
                '-hda', self.disk_filename,
                '-device', 'virtio-gpu',
                '-boot', 'n',
                '-nic', f'bridge,br={BRIDGE},mac={self.mac},model=virtio-net-pci',
                '-nographic',
                '-chardev',
                f'socket,id=saladtcp,host=localhost,port={SALAD_TCP_CONSOLE_PORT},server=off,logfile={log_name},mux=on',
                '-device', 'pci-serial,chardev=saladtcp',
                '-serial', 'chardev:saladtcp',
            ] + extra_cmds

            log('starting DUT: %s', ' '.join(cmd))
            qemu = subprocess.Popen(cmd)

            # Add a new window in the dashboard to monitor the DUT logs
            # -d: do not select window
            # -a: insert after target window
            # -t: target window
            # -n: window name
            subprocess.Popen(['tmux', 'new-window', '-d', '-a', '-t', 'dashboard',
                              '-n', f'dut-{self.id}', 'tail', '-F', log_name])
        except Exception:
            traceback.print_exc()

        # Wait for the stop signal before killing qemu
        self.stop_event.wait()

        # Don't try to be nice, kill the (virtual) machine just like it would be in the real life
        try:
            qemu.kill()
        except Exception:
            traceback.print_exc()

        # Remove the window in the dashboard that monitored the DUT logs
        try:
            subprocess.Popen(['tmux', 'kill-window', '-t', f'dut-{self.id}'])
        except Exception:
            traceback.print_exc()

    def stop(self):
        self.stop_event.set()

    def teardown(self):
        self.stop()
        self.thread.join(5)
        try:
            os.remove(self.disk_filename)
        except OSError as e:
            print(f"Can't remove the disk file '{self.disk_filename}': {e}", file=sys.stderr)


class PDUTCPHandler(socketserver.StreamRequestHandler):
    def handle(self):
        log("client: %s", self.client_address[0])
        try:
            data = self.rfile.read(4)
            if len(data) != 4:
                raise ValueError
            payload = int.from_bytes(data[:4], byteorder='big')
            log("payload: %s  %s", hex(payload), bin(payload))
        except ValueError:
            log("Bad input: %s", data)
            self.wfile.write(b'\x00')
            self.request.close()
            return

        # Protocol
        # [0:1] operation
        # [2:12] PDU port (0-1023)
        # [13:31] reserved, MUST BE CHECKED TO BE 0s
        # E.g., turn 2 off
        #   echo -e '\x00\x00\x00\x0A' | nc localhost 9191
        # And then on again,
        #   echo -e '\x00\x00\x00\x09' | nc localhost 9191
        cmd = payload & 0x03
        port = (payload & 0x1FFC) >> 2
        shutdown = (payload & 0x2000) == 0x2000
        reserved = (payload & 0xFFFFC000) >> 13
        assert (reserved == 0)

        if shutdown:
            setattr(self.server, '_BaseServer__shutdown_request', True)

        if not (port >= 0 and port < len(OUTLETS)):
            log("port %d out of range", port)
            self.wfile.write(b'\x00')
            return

        machine = OUTLETS[port]
        assert (machine)

        log("cmd=%s on port=%d", hex(cmd), port)

        if cmd & 0x03 == 3:
            log("status for port=%d", port)
            self.wfile.write(struct.pack('!B', int(machine.state)))
        elif cmd & 0x01:
            log("turning port=%d ON", port)
            machine.start()
            self.wfile.write(b'\x01')
            log("port=%d turned ON", port)
        elif cmd & 0x02:
            log("turning port=%d OFF", port)
            machine.stop()
            self.wfile.write(b'\x01')
            log("port %d turned OFF", port)
        else:
            assert (cmd == 0)
            log("return num ports")
            self.wfile.write(struct.pack('!B', len(OUTLETS)))


if __name__ == "__main__":
    class MachineConfig:
        def __init__(self, cfg):
            arch, firmware, protocol = cfg.split('-')

            self.cfg = cfg
            self.arch = Architecture[arch.upper()]
            self.firmware = Firmware[firmware.upper()]
            self.protocol = NetbootProtocol[protocol.upper()]

        def __str__(self):
            return self.cfg

        def __repr__(self):
            return self.cfg

        @classmethod
        def from_argument(cls, cfg):
            port, m_cfg = cfg.split(':')
            port = int(port)
            if port < 0:
                raise ValueError("Negative ports are invalid")
            return (port, cls(m_cfg))

    parser = argparse.ArgumentParser(prog='Virtual PDU')
    parser.add_argument('--host', default='localhost')
    parser.add_argument('--port', default=9191, type=int)
    parser.add_argument('--num-ports', default=NUM_PORTS, type=int)
    parser.add_argument('--log-file', default='vpdu.log', type=str)
    parser.add_argument('--log-level', default='INFO', type=str)
    parser.add_argument('--salad-console-port', default=SALAD_TCP_CONSOLE_PORT, type=int)
    parser.add_argument('--dut-disk-size', default=DUT_DISK_SIZE, type=str,
                        help=f'In the format expected by qemu-img. Default {DUT_DISK_SIZE}')
    parser.add_argument('--bridge', default=BRIDGE)
    parser.add_argument('--default-port-cfg', default=DEFAULT_MACHINE_CFG, type=MachineConfig,
                        help=f'The default machine config. Default {DEFAULT_MACHINE_CFG}')
    parser.add_argument('--port-cfg', '-c', action='append', type=MachineConfig.from_argument,
                        dest="ports_cfg", default=[])

    args = parser.parse_args()

    SALAD_TCP_CONSOLE_PORT = args.salad_console_port
    DUT_DISK_SIZE = args.dut_disk_size
    BRIDGE = args.bridge
    args.ports_cfg = dict(args.ports_cfg)
    OUTLETS = [DUT(mac=gen_mac(i), machine_cfg=args.ports_cfg.get(i, args.default_port_cfg),
                   fw_var_filename=f'{VPDU_DIR}/efi_var{i}.fd',
                   disk_filename=f'{VPDU_DIR}/dut_disk{i}.qcow2') for i in range(args.num_ports)]

    print("\nOutlets:")
    for i, outlet in enumerate(OUTLETS):
        print(f"{i}: {outlet.machine_cfg}")
    print()

    socketserver.TCPServer.allow_reuse_address = True
    try:
        log("OK. Waiting for connections...")
        with socketserver.TCPServer((args.host, args.port), PDUTCPHandler) as server:
            server.allow_reuse_address = True
            server.serve_forever()
    except KeyboardInterrupt:
        log("Interrupt")
        pass
    finally:
        for machine in OUTLETS:
            machine.teardown()
