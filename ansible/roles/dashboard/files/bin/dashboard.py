#!/usr/bin/env python3
# coding: utf-8

import json
import subprocess
import urllib.request
import urllib.parse
import urwid

# Custom urwid classes
import curwid
from curwid import ColorButton

from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument('-s', '--services', nargs='+', default=[])
args = parser.parse_args()

host = "http://localhost"
networking_list = ["public", "private", "mgmt", "wg0"]
services_list = args.services


def networking_data():
    def show_nic(jnic, depth=0):
        nic_name = jnic.get('ifname')
        status = jnic.get('operstate')

        if depth == 0:
            ips = jnic.get('addr_info', [])
            if len(ips) > 0:
                ip = ips[0].get('local')
            else:
                ip = "<no address>"
        else:
            ip = ""

        if status == "UNKNOWN":
            status = "???"
            color = "yellow"
        elif status == "UP":
            color = "green"
        else:
            color = "brown"

        prefix = (depth + 1) * " "
        if depth > 0:
            prefix += '- '

        widgets.append(urwid.AttrWrap(urwid.Text(f"{prefix}{nic_name: <8} {status: <4} {ip}"), color))

        if jnic.get('linkinfo', {}).get('info_kind') == 'bridge':
            attached_nics_str = subprocess.getoutput(f"ip --json --detail link show master {nic_name}")

            try:
                attached_nics = json.loads(attached_nics_str)
            except Exception:
                widgets.append(urwid.AttrWrap(urwid.Text("  - failed to list attached NICs"), "red"))
                return

            for s_jnic in attached_nics:
                show_nic(s_jnic, depth=depth+1)

    def get_nic_details(output):
        info = subprocess.getoutput(f"ip --json --detail addr show {nic}")

        if "does not exist" in info:
            return None, "MISSING"

        try:
            jnics = json.loads(info)
            if len(jnics) > 0:
                return jnics[0], "OK"
            else:
                return None, "MISSING"
        except Exception:
            return None, "ERROR"

    widgets = []
    for nic in networking_list:
        jnic, status = get_nic_details(nic)
        if jnic:
            show_nic(jnic)
        else:
            widgets.append(urwid.AttrWrap(urwid.Text(f" {nic: <8} {status: <4}"), "red"))

    return urwid.Pile(widgets)


def services_data():
    def expand_service_pattern(service_pattern):
        try:
            services = []
            serv_matched_str = subprocess.getoutput(f"systemctl list-unit-files '{service_pattern}.service' -o json")
            for unit_file_matched in json.loads(serv_matched_str):
                services.append(unit_file_matched['unit_file'].removesuffix(".service"))
            return services if len(services) > 0 else None
        except Exception:
            return None

    def add_widget(service, status, color):
        widgets.append(urwid.AttrWrap(urwid.Text(f" {service: <20} {status}"), color))

    widgets = []
    for service_pattern in services_list:
        # If an expectation is set, remove it
        if ":" in service_pattern:
            pattern, expected = service_pattern.split(":")
        else:
            pattern = service_pattern
            expected = "active"

        # NOTE: We expand the list of services every update, as new services
        # may have appeared using live-provisioning
        expanded_services = expand_service_pattern(pattern)

        if expanded_services is None:
            add_widget(pattern, "missing", "red")
        else:
            for serv in expanded_services:
                status = subprocess.getoutput(f"systemctl is-active {serv}")

                if status == "activating":
                    color = "yellow"
                elif status == expected:
                    color = "green"
                    status = "OK"
                else:
                    color = "red"

                add_widget(serv, status, color)

    return urwid.Pile(widgets)


def fetch_full_state():
    dashboard = {}
    discover = {}
    wait_for_config = []

    try:
        with urllib.request.urlopen(f'{host}/api/v1/full-state') as resp:
            fs = json.loads(resp.read())
    except urllib.error.URLError as e:
        return discover, dashboard, f"urllib error requesting full-state: {e.reason}"
    except Exception as e:
        return discover, dashboard, f"Unexpected { e.__class__} in fetch_full_state: {e}"

    if not fs or ("pdus" not in fs) or ("duts" not in fs) or ("discover" not in fs):
        return discover, dashboard, f"error reading /full-state endpoint, query returned: {fs}"

    discover = fs['discover']

    # If we don't have at least a PDU, we have nothing to show
    if len(fs.get("pdus")) == 0:
        return discover, dashboard, "No PDUs were found"

    # Assign ports to each pdu
    # Take advantage to rename state to port-state
    for pdu, ports in fs["pdus"].items():
        modif_ports = {}
        for p, info in ports["ports"].items():
            info = {"port-state" if k == "state" else k: v for k, v in info.items()}
            modif_ports[p] = info
        dashboard[pdu] = modif_ports

    if not dashboard:
        return discover, dashboard, f"error reading pdu ports when creating the dashboard: {dashboard}"

    for mac, dut in fs["duts"].items():
        name = dut["pdu"]["name"]
        port_id = dut["pdu"]["port_id"]

        # Handle duts without PDU configuration
        if name is None or port_id is None:
            wait_for_config.append({"ip_address": dut["ip_address"],
                                    "mac_address": dut["mac_address"],
                                    })
        else:
            dashboard[name][port_id].update(dut)

    if wait_for_config:
        pdu_name = "Unknown, fix your configuration file manually"
        dashboard[pdu_name] = {}
        fake_port_id = 100
        for m in wait_for_config:
            dashboard[pdu_name][fake_port_id] = m
            fake_port_id += 1

    return discover, dashboard, ""


def execute_request(url, data=None, method="POST"):
    if data:
        if method == "PATCH":
            data = json.dumps(data).encode()
        headers = {'Content-Type': 'application/json'}
        request = urllib.request.Request(url,
                                         data=data,
                                         headers=headers,
                                         method=method)
    else:
        request = urllib.request.Request(url, method=method)

    try:
        resp = urllib.request.urlopen(request, timeout=10)
    except urllib.error.URLError as e:
        if data is None:
            return None, f"There was some unexpected issue ({e.reason})"
        else:
            return None, f"There is a discovery process running already ({e.reason})"
    except Exception as e:
        return None, f"Unexpected { e.__class__} in execute_request: {e}"

    return resp, resp.read()


class Dashboard:
    palette = [
        ('body', 'light gray', 'black', 'standout'),
        ('header', 'dark blue', 'black', 'bold'),
        ('pdu', 'black', 'dark blue', ('standout', 'underline')),
        ('bttn_discover', 'black', 'dark green'),
        ('bttn_cancel', 'black', 'dark red'),
        ('bttn_retire', 'black', 'yellow'),
        ('buttn_activate', 'black', 'dark cyan'),
        ('buttn_info', 'black', 'light green'),
        ('buttn_default', 'black', 'dark green'),
        ('buttnf', 'white', 'dark blue', 'bold'),
        # Colors for text
        ('red', 'dark red', 'black'),
        ('brown', 'brown', 'black'),
        ('yellow', 'yellow', 'black'),
        ('green', 'dark green', 'black'),
        ('light blue', 'light blue', 'black'),
        ]

    def unhandled_input(self, key):
        if key in ('q', 'Q'):
            raise urwid.ExitMainLoop()

    def button_actions(self, button, data):
        button = button.get_label()
        mac = data.get('machine').get('mac_address')
        pdu_name = data.get('pdu')
        port_id = data.get('port')

        if button == "DISCOVER":
            post_dict = {
                "pdu": data.get('pdu'),
                "port_id": data.get('port')
                }
            url = f"{host}/api/v1/dut/discover"
            post_data = json.dumps(post_dict).encode()
            resp, message = execute_request(url, post_data)
        elif (button == "CANCEL" and data.get('machine').get('port-state') == 'ON'):
            url = f"{host}/api/v1/dut/discover"
            resp, message = execute_request(url, data=None, method="DELETE")
        elif button == "DELETE MACHINE":
            url = f"{host}/api/v1/dut/{mac}"
            resp, message = execute_request(url, data=None, method="DELETE")
        elif "PORT" in button:
            if button == "UNRESERVE PORT":
                d = {"reserved": "False"}
            elif button == "RESERVE PORT":
                d = {"reserved": "True"}
            elif button == "TURN PORT OFF":
                d = {"state": "off"}
            elif button == "TURN PORT ON":
                d = {"state": "on"}

            url = f"{host}/api/v1/pdu/{pdu_name}/port/{port_id}"
            resp, message = execute_request(url, d, method="PATCH")

        else:
            mac = data.get('machine').get('mac_address')
            if button == "RETIRE":
                url = f"{host}/api/v1/dut/{mac}/retire"
            elif button == "ACTIVATE":
                url = f"{host}/api/v1/dut/{mac}/activate"
            elif button == "CANCEL":
                url = f"{host}/api/v1/dut/{mac}/cancel_job"
            elif button == "RETRAIN":
                url = f"{host}/api/v1/dut/{mac}/retrain"
            elif button == "SKIP TRAINING":
                url = f"{host}/api/v1/dut/{mac}/skip_training"
            elif button == "QUEUE QUICK CHECK":
                url = f"{host}/api/v1/dut/{mac}/quick_check"
            resp, message = execute_request(url)

        if type(message) == bytes:
            message = ''.join(map(chr, message))
        self.loop.widget.footer = urwid.AttrWrap(urwid.Text(" " + button + ": " + message), "header")

    def button_on_machine_view(self, button, data):
        self.button_actions(button, data)
        self.button_close_machine_view(button, data)

    def button_close_machine_view(self, button, data):
        self.loop.widget.body = self.create_body_main_view()
        self.start_alarm()

    def button_save_close_machine_view(self, button, data):
        mac = data.get('machine').get('mac_address')
        pdu_off_delay = data.get('machine').get('pdu_off_delay')
        comment = data.get('machine').get('comment')

        d = {}
        if pdu_off_delay != self.databox.pdu_off_delay:
            d.update({"pdu_off_delay": self.databox.pdu_off_delay})

        if comment != self.databox.comment:
            d.update({"comment": self.databox.comment})

        if d:
            url = f"{host}/api/v1/dut/{mac}"
            resp, message = execute_request(url, d, method="PATCH")
            self.loop.widget.footer = urwid.AttrWrap(urwid.Text(message), "header")

        self.loop.widget.body = self.create_body_main_view()
        self.start_alarm()

    def expert_mode_button(self, button, state, data):
        if not button.get_state():
            w = ColorButton("DELETE MACHINE", self.button_on_machine_view, data)
        else:
            w = urwid.Divider()

        self.expert_buttons_wrap._w = w

    def show_log_view(self, button, data):
        logs_endoint = data.get('machine').get('logs_endpoint')
        host_log = logs_endoint.get('host')
        port_log = logs_endoint.get('port')

        if not port_log or not host_log:
            return

        cmd = ['nc', str(host_log), str(port_log)]
        term = urwid.Terminal(cmd, encoding='utf-8')
        urwid.connect_signal(term, 'closed', self.create_body_machine_port_view, data)
        term.main_loop = self.loop

        self.loop.widget.body = urwid.LineBox(term, title='Use CONTROL+C to exit')

    def create_body_machine_port_view(self, button, data):
        self.stop_alarm()

        machine = data.get('machine')
        content = [urwid.Divider()]
        buttons = [ColorButton("Return", self.button_close_machine_view, data),
                   ColorButton("Save changes & return", self.button_save_close_machine_view, data),
                   urwid.Divider()]

        if machine.get('mac_address') is None:
            if machine.get('reserved'):
                buttons.append(ColorButton("UNRESERVE PORT", self.button_on_machine_view, data))
            else:
                buttons.append(ColorButton("RESERVE PORT", self.button_on_machine_view, data))
                if machine.get('port-state') == 'ON':
                    buttons.append(ColorButton("TURN PORT OFF", self.button_on_machine_view, data))
                if machine.get('port-state') == 'OFF':
                    buttons.append(ColorButton("TURN PORT ON", self.button_on_machine_view, data))

        else:
            buttons.append(ColorButton("SHOW LOGS", self.show_log_view, data))
            if machine.get('ready_for_service'):
                buttons.append(ColorButton("RETRAIN", self.button_on_machine_view, data))
            else:
                buttons.append(ColorButton("SKIP TRAINING", self.button_on_machine_view, data))

            buttons.append(ColorButton("QUEUE QUICK CHECK", self.button_on_machine_view, data))

            if machine.get('state') == 'RETIRED':
                if machine.get('port-state') == 'OFF':
                    buttons.append(ColorButton("TURN PORT ON", self.button_on_machine_view, data))
                else:
                    buttons.append(ColorButton("TURN PORT OFF", self.button_on_machine_view, data))

            buttons.append(urwid.CheckBox("Expert mode", on_state_change=self.expert_mode_button, user_data=data))
            # Create a placeholder for expert buttons
            self.expert_buttons_wrap = urwid.WidgetWrap(urwid.Divider())
            buttons.append(self.expert_buttons_wrap)

        self.databox = curwid.MachinePortTable(machine)
        col_one = ('fixed', 55, self.databox)
        col_two = ('fixed', 25, urwid.Pile(buttons))
        content.append(urwid.Columns([col_one, col_two], dividechars=2))
        content.append(urwid.Divider())
        title_box = data.get('pdu') + " " + str(data.get('port'))
        self.loop.widget.body = urwid.LineBox(urwid.ListBox(urwid.SimpleListWalker(content)), title=title_box)

        # Force default focus on button "Return to main screen"
        self.loop.widget.body.original_widget.keypress((0, 30), 'right')

    def create_body_main_view(self):

        discover, dashboard, error_message = fetch_full_state()

        if dashboard:
            # Make list to feed to ListBox
            listbox_content = []

            for pdu, ports in dashboard.items():
                listbox_content.append(urwid.Divider())
                listbox_content.append(urwid.Padding(
                    urwid.Text(("pdu", f"PDU name : {pdu}")), left=1, right=0, min_width=20))

                line = []
                for num, machine in ports.items():
                    # Return machine state or port state if there isn't a machine
                    state = machine.get('state') or machine.get('port-state')

                    led = "○"
                    if machine.get('port-state') == 'ON':
                        led = "●"

                    line = [('fixed', 14, urwid.Text(f" {led} Port {num}:"))]

                    if state == "TRAINING":
                        boot_loop_counts = machine.get('training').get('boot_loop_counts')
                        current_loop_count = machine.get('training').get('current_loop_count')
                        stext = f"{state} {current_loop_count}/{boot_loop_counts}"
                    elif (state == "ON" and discover and discover.get('port_id') == num and discover.get('pdu') == pdu):
                        stext = "DISCOVERING"
                    else:
                        stext = f"{state}"
                    line.append(('fixed', 16, urwid.Text(stext)))

                    name = machine.get('full_name', "")
                    line.append(urwid.Text(name))

                    button_data = {
                        "pdu": pdu,
                        "port": num,
                        "machine": machine,
                        }

                    line.append(('fixed', 10,
                                ColorButton("MORE", self.create_body_machine_port_view, button_data, 'buttn_info')))

                    if machine.get('reserved'):
                        line.append(('fixed', 14, urwid.Text("Reserved port")))
                    elif state == "OFF":
                        line.append(('fixed', 14,
                                     ColorButton("DISCOVER", self.button_actions, button_data, 'bttn_discover')))
                    elif state in ["IDLE", "TRAINING"] and not machine.get('is_retired'):
                        line.append(('fixed', 14,
                                    ColorButton("RETIRE", self.button_actions, button_data, 'bttn_retire')))
                    elif machine.get('is_retired'):
                        line.append(('fixed', 14,
                                    ColorButton("ACTIVATE", self.button_actions, button_data, 'buttn_activate')))
                    # Button to cancel running job
                    elif state == "RUNNING":
                        line.append(('fixed', 14,
                                    ColorButton("CANCEL", self.button_actions, button_data, 'bttn_cancel')))
                    # Button to cancel discover process
                    elif (state == "ON" and discover and discover.get('port_id') == num and discover.get('pdu') == pdu):
                        line.append(('fixed', 14,
                                    ColorButton("CANCEL", self.button_actions, button_data, 'bttn_cancel')))
                    else:
                        line.append(('fixed', 14, urwid.Text("")))

                    listbox_content.append(urwid.Columns(line, min_width=10, dividechars=1))

            self.machines_box = True
        else:
            self.machines_box = False
            listbox_content = [urwid.Text(error_message)]

        net = urwid.LineBox(networking_data(), title="Networking")
        ser = urwid.LineBox(services_data(), title="Services")
        col_two = urwid.ListBox([net, ser])
        col_one = urwid.LineBox(urwid.ListBox(urwid.SimpleListWalker(listbox_content)), title="PDUs")
        columns = urwid.Columns([(col_one), ('fixed', 34, col_two)], dividechars=2)
        return columns

    def stop_alarm(self):
        self.loop.remove_alarm(self.refresh_alarm)
        self.refresh_alarm = None

    def start_alarm(self):
        self.refresh_alarm = self.loop.set_alarm_in(2, self.refresh)

    def main(self):
        header_line = " Control this dashboard with your mouse or " \
            "keys UP / DOWN / PAGE UP / PAGE DOWN / ENTER. Use Q to exit.\n"
        frame = urwid.Frame(header=urwid.AttrWrap(urwid.Text(header_line), 'header'),
                            body=self.create_body_main_view(),
                            footer=urwid.AttrWrap(urwid.Text(""), "header"))

        self.loop = urwid.MainLoop(widget=frame,
                                   palette=self.palette,
                                   unhandled_input=self.unhandled_input)
        self.start_alarm()
        self.loop.run()

    def refresh(self, loop=None, data=None):
        focus = self.loop.widget.body.widget_list[0].original_widget.get_focus_path()
        # Cleaning the CanvasCache here is necessary to remove all the extra reference to widgets
        # This will allow the garbage collector to do its job and remove old widgets.
        urwid.CanvasCache.clear()
        self.loop.widget.body = self.create_body_main_view()
        self.start_alarm()

        if self.machines_box:
            try:
                self.loop.widget.body.widget_list[0].original_widget.set_focus_path(focus)
                # Workaround when the focus is on the second column
                if len(focus) == 2 and focus[1] == 4:
                    self.loop.widget.body.widget_list[0].original_widget.keypress((0, 1), 'right')
            # Make sure we always put the focus somewhere, in case of unknown focus
            # set the focus to the position of the first MORE button on [2,3]
            except IndexError:
                focus = [2, 3]
                self.loop.widget.body.widget_list[0].original_widget.set_focus_path(focus)


if '__main__' == __name__:
    Dashboard().main()
