# Valve CI Infrastructure

This repository contains the source for the Valve CI infrastructure. Its main
purpose is to build a multi-service container that acts as a bare-metal CI
gateway service, responsible for the orchestration and management of devices
under test, or DUTs as we call them.

Take a look to our documentation at
[https://mupuf.pages.freedesktop.org/valve-infra/](https://mupuf.pages.freedesktop.org/valve-infra/)

