import contextlib
import io
import unittest

from gfxinfo import SUPPORTED_GPU_DBS, PCIDevice, DeviceTreeGPU


class DatabaseTests(unittest.TestCase):
    def test_check_db(self):
        for gpu_db in SUPPORTED_GPU_DBS:
            with self.subTest(GPU_DB=type(gpu_db).__name__):
                self.assertTrue(gpu_db.check_db())


class PCIDeviceTests(unittest.TestCase):
    def test_hash(self):
        self.assertEqual(hash(PCIDevice(0x1234, 0x5678, 0x9a)), 0x123456789a)

    def test_str(self):
        self.assertEqual(str(PCIDevice(0x1234, 0x5678, 0x9a)), "0x1234:0x5678:0x9a")

    def test_from_str(self):
        self.assertEqual(PCIDevice.from_str("1234:5678:9a"), PCIDevice(0x1234, 0x5678, 0x9a))
        self.assertEqual(PCIDevice.from_str("0x1234:0x5678:0x9a"), PCIDevice(0x1234, 0x5678, 0x9a))

        self.assertEqual(PCIDevice.from_str("0x1234:5678"), PCIDevice(0x1234, 0x5678, 0x0))

        with self.assertRaises(ValueError):
            self.assertEqual(PCIDevice.from_str("0x1234:5678:0x12:045"), PCIDevice(0x1234, 0x5678, 0x0))


class DeviceTreeGPUTests(unittest.TestCase):
    def setUp(self):
        self.gpu = DeviceTreeGPU.from_compatible_str("brcm,bcm2711-vc5\0brcm,bcm2835-vc4\0")

    def test_base_name(self):
        self.assertEqual(self.gpu.base_name, "brcm_bcm2711-vc5")

    def test_pciid(self):
        self.assertIsNone(self.gpu.pciid)

    def test_tags(self):
        self.assertEqual(self.gpu.tags, {"dt_gpu:vendor:brcm", "dt_gpu:model:bcm2711-vc5"})

    def test_structured_tags(self):
        self.assertEqual(self.gpu.structured_tags,
                         {"type": "devicetree",
                          "vendor": "brcm",
                          "model": "bcm2711-vc5"})

    def test_str(self):
        self.assertEqual(str(self.gpu), "<DeviceTreeGPU: brcm/bcm2711-vc5>")

    def test_from_compatible_str(self):
        f = io.StringIO()
        with contextlib.redirect_stderr(f):
            self.assertIsNone(DeviceTreeGPU.from_compatible_str("brcm,bcm2711-vc5,extra"))

        self.assertEqual(f.getvalue(), ("ERROR: The compatible 'brcm,bcm2711-vc5,extra' is not "
                                        "following the expected format 'vendor,model'\n"))
