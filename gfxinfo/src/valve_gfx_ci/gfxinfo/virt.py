from dataclasses import dataclass

from . import PCIDevice


@dataclass
class VirtGPU:
    pci_device: PCIDevice

    @property
    def base_name(self):
        return 'virtio'

    @property
    def pciid(self):
        return str(self.pci_device)

    @property
    def tags(self):
        return {
            f"virtio:pciid:{self.pciid}",
            "virtio:family:VIRTIO",
        }

    @property
    def structured_tags(self):
        return {
            "type": "virtio"
        }

    def __str__(self):
        return f"<VirtGPU: PCIID {self.pciid}>"


class VirtIOGpuDeviceDB:
    def cache_db(self):
        # NOTHING TO DO
        pass

    def check_db(self):
        return True

    def update(self):
        # NOTHING TO DO
        pass

    def from_pciid(self, pciid):
        if pciid.vendor_id != 0x1af4 and pciid.product_id != 0x1050:
            return None
        else:
            return VirtGPU(pci_device=pciid)
