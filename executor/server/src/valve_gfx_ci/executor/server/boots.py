from dataclasses import dataclass
from urllib.request import urlretrieve
from threading import Thread
import os
import logging

from . import config
from .tftpd import TFTPD
from .dhcpd import DHCPD, CPUArch, Firmware, BootProtocol, MacAddress
from .logger import logger

DEFAULT_CONFIG_PATHS = {
    'TFTP_DIR': '/boots/tftp',
}

# The IPXE binaries can get generated using "make ipxe-dut-clients"
IPXE_BASE_URL = 'https://downloads.gfx-ci.steamos.cloud/ipxe-dut-client/'
IPXE_BINARIES = {
    "IPXE_X86_64_EFI_FILENAME": '2023-02-08_13-38-07-mupuf-x86_64-snponly.efi',
    "IPXE_I386_EFI_FILENAME": '2023-02-08_13-38-07-mupuf-i386-snponly.efi',
    "IPXE_I386_MBR_FILENAME": '2023-02-08_13-38-07-mupuf-i386-undionly.kpxe',
    "IPXE_ARM32_EFI_FILENAME": '2023-02-08_13-38-07-mupuf-arm32-snponly.efi',
    "IPXE_ARM64_EFI_FILENAME": '2023-02-08_13-38-07-mupuf-arm64-snponly.efi',
}
BASE_DIR = os.path.dirname(__file__)


def provision_ipxe_dut_clients(tftp_dir):  # pragma: nocover
    os.makedirs(tftp_dir, exist_ok=True)

    logger.debug("Downloading the latest iPXE DUT clients...")
    for filename in IPXE_BINARIES.values():
        urlretrieve(f"{IPXE_BASE_URL}/{filename}", os.path.join(tftp_dir, filename))


class Dhcpd(DHCPD, Thread):
    def __init__(self, boots, name, interface):  # pragma: nocover
        self.boots = boots

        Thread.__init__(self, name=name)
        self.daemon = True

        DHCPD.__init__(self, interface=interface)

    @property
    def static_clients(self):
        clients = []
        for dut in self.boots.mars.known_machines:
            clients.append({'mac_addr': MacAddress(dut.mac_address),
                            'ipaddr': dut.ip_address,
                            'hostname': dut.full_name})
        return clients

    def boot_target(self, client_request):
        if client_request.architecture not in [CPUArch.X86, CPUArch.X86_64, CPUArch.ARM32, CPUArch.ARM64]:
            self.logger.error(f"Unsupported architecture '{client_request.architecture}'")
            return None

        if client_request.protocol != BootProtocol.TFTP:
            self.logger.error(f"Unsupported protocol '{client_request.protocol}'")
            return None

        if client_request.user_class == "iPXE":
            p = client_request.firmware.to_ipxe_platform if client_request.firmware else ""
            b = client_request.architecture.to_ipxe_buildarch if client_request.architecture else ""
            return f"{config.EXECUTOR_URL}/boot/{client_request.mac_addr}/boot.ipxe?platform={p}&buildarch={b}"
        elif client_request.firmware == Firmware.BIOS:
            if client_request.architecture in [CPUArch.X86, CPUArch.X86_64]:
                # NOTE: BIOS firmware means an x86-compatible CPU, and the i386 binary will work for all of them
                return IPXE_BINARIES['IPXE_I386_MBR_FILENAME']
            else:
                self.logger.error(f"Unsupported BIOS architecture '{client_request.architecture}'")
        elif client_request.firmware == Firmware.UEFI:
            if client_request.architecture == CPUArch.X86:
                return IPXE_BINARIES['IPXE_I386_EFI_FILENAME']
            elif client_request.architecture == CPUArch.X86_64:
                return IPXE_BINARIES['IPXE_X86_64_EFI_FILENAME']
            elif client_request.architecture == CPUArch.ARM32:
                return IPXE_BINARIES['IPXE_ARM32_EFI_FILENAME']
            elif client_request.architecture == CPUArch.ARM64:
                return IPXE_BINARIES['IPXE_ARM64_EFI_FILENAME']
            else:  # pragma: nocover
                self.logger.error(f"Unsupported UEFI architecture '{client_request.architecture}'")
        else:
            self.logger.error(f"Unsupported firmware type '{client_request.firmware}'")
            return None

    def run(self):  # pragma: nocover
        self.listen()


class Tftpd(Thread):  # pragma: nocover
    def __init__(self, boots, name, directory, interface):
        self.boots = boots

        Thread.__init__(self, name=name)
        self.daemon = True

        tftp_logger = logging.getLogger(f"{logger.name}.TFTP")
        tftp_logger.setLevel(logging.INFO)
        self.tftp_server = TFTPD(interface, logger=tftp_logger, netboot_directory=directory)

    def run(self):
        self.tftp_server.listen()


@dataclass
class BootConfig:
    kernel: str
    initrd: str
    cmdline: str

    @classmethod
    def _gen_envvar_name(cls, suffix, fields):
        # Check that all the fields are strings
        for field in fields:
            if not isinstance(field, str):
                return None

        if len(fields) > 0:
            fields_str = "_".join([f.upper() for f in fields if f]) + "_"
        else:
            fields_str = ""

        return "BOOTS_DEFAULT_" + fields_str + suffix

    @classmethod
    def _find_best_option(cls, machine, bootloader, platform, buildarch, suffix):
        # Generate all the possible names for environment variables
        prioritized_combos = [
            (bootloader, buildarch, platform),
            (buildarch, platform),
            (bootloader, buildarch),
            (buildarch, ),
            (bootloader, ),
            tuple()
        ]
        prioritized_opts = [cls._gen_envvar_name(suffix, fields) for fields in prioritized_combos]

        # Iterate through the options, in priority order, ignoring invalid entries
        for option in [o for o in prioritized_opts if o]:
            if opt := os.environ.get(option):
                return opt

    @classmethod
    def defaults(cls, machine=None, bootloader=None, platform=None, buildarch=None):
        # Possible values:
        # * bootloader: ipxe
        # * platforms: efi, pcbios
        # * buildarch: i386, x86_64, arm32, arm64

        kwargs = {"machine": machine, "bootloader": bootloader, "platform": platform, "buildarch": buildarch}
        kernel = cls._find_best_option(**kwargs, suffix="KERNEL")
        if not kernel:
            raise ValueError("No default kernel found (specified using BOOTS_DEFAULT_*KERNEL)")

        initrd = cls._find_best_option(**kwargs, suffix="INITRD")
        if not initrd:
            raise ValueError("No default initramfs found (specified using BOOTS_DEFAULT_*INITRD)")

        cmdline = cls._find_best_option(**kwargs, suffix="CMDLINE")
        if not cmdline:
            raise ValueError("No default kernel command line found (specified using BOOTS_DEFAULT_*CMDLINE)")

        return cls(kernel=kernel, initrd=initrd, cmdline=cmdline)

    def fixup_missing_fields_with_defaults(self, machine=None, bootloader=None, platform=None, buildarch=None):
        kwargs = {"machine": machine, "bootloader": bootloader, "platform": platform, "buildarch": buildarch}

        if not self.kernel:
            self.kernel = self._find_best_option(**kwargs, suffix="KERNEL")
            if not self.kernel:
                raise ValueError("No default kernel found (specified using BOOTS_DEFAULT_*KERNEL)")

        if not self.initrd:
            self.initrd = self._find_best_option(**kwargs, suffix="INITRD")
            if not self.initrd:
                raise ValueError("No default initramfs found (specified using BOOTS_DEFAULT_*INITRD)")


class BootService:
    def __init__(self, mars,
                 private_interface=None,
                 config_paths=DEFAULT_CONFIG_PATHS):
        self.mars = mars
        self.private_interface = private_interface or config.PRIVATE_INTERFACE
        self.config_paths = config_paths

        # Do not start the servers
        if config.BOOTS_DISABLE_SERVERS:
            self.dhcpd = self.tftpd = None
            return

        # Download the iPXE binaries and store them where the DUTs can download them
        provision_ipxe_dut_clients(tftp_dir=config_paths['TFTP_DIR'])

        self.dhcpd = Dhcpd(self, "DHCP Server", self.private_interface)
        self.dhcpd.dns_servers = [self.dhcpd.ip]
        if not config.BOOTS_DISABLE_SERVERS:
            self.dhcpd.start()

        self.tftpd = Tftpd(self, "TFTP Server", self.config_paths["TFTP_DIR"], self.private_interface)
        if not config.BOOTS_DISABLE_SERVERS:
            self.tftpd.start()

    @classmethod
    def _platform_cmdline(cls, platform=None):
        return "initrd=initrd" if platform != "pcbios" else ""

    @classmethod
    def _gen_ipxe_boot_script(cls, bootconfig, platform=None):
        platform_cmdline = cls._platform_cmdline(platform=platform)

        cmdline = bootconfig.cmdline if bootconfig.cmdline is not None else ""
        cmdline = cmdline.replace(";", "${semicolon:string}")
        cmdline = cmdline.replace("&", "${ampersand:string}")
        cmdline = cmdline.replace("|", "${pipe:string}")

        return f"""#!ipxe

set semicolon:hex 3b
set ampersand:hex 26
set pipe:hex 7C

echo

echo Downloading the kernel
kernel {bootconfig.kernel} {platform_cmdline} {cmdline}

echo Downloading the initrd
initrd --name initrd {bootconfig.initrd}

echo Booting!
boot
"""

    def ipxe_boot_script(self, machine=None, platform=None, buildarch=None):
        bootconfig = None

        if machine is not None:
            bootconfig = machine.boot_config_query(platform=platform, buildarch=buildarch, bootloader="ipxe")

        if bootconfig is None:
            bootconfig = BootConfig.defaults(machine=machine, bootloader="ipxe",
                                             platform=platform, buildarch=buildarch)

        return self._gen_ipxe_boot_script(bootconfig, platform=platform)
